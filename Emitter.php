<?php
namespace vlka\ws;

/**
 * Class Emitter
 * @package vlka\ws
 */
class Emitter
{
    const EVENT_EMIT_ALL = '_emit_all_';
    const EVENT_TO = '_emit_to_';

    protected $_rooms = [];
    protected $_client;
    protected $_salt;

    public $url;

    /**
     * Emitter constructor.
     * @param string $port
     * @param string $salt
     */
    public function __construct($port = '', $salt = '')
    {
        if(is_numeric($port)){
            $this->url = $_SERVER['HTTP_HOST'] . ':' . $port . '/socket.io?EIO=3&transport=websocket';
        } else {
            $this->url = $port;
        }
        $this->_salt = $salt;
    }

    /**
     * @param string $name
     * @param mixed $data
     * @throws \Exception
     */
    public function emit($name, $data)
    {
        $dataString = '42';
        if($this->_rooms){
            $event = self::EVENT_TO;
            $dataString .= json_encode([$event, ['rooms' => $this->_rooms, 'name' => $name, 'data' => $data]]);
            $this->_rooms = [];
        } else {
            $event = self::EVENT_EMIT_ALL;
            $dataString .= json_encode([$event, ['name' => $name, 'data' => $data]]);
        }
        $hesh = $this->generateHesh($event);

        $this->_client = new Client($this->url . '&hesh=' . $hesh);
        $this->_client->write($dataString);
    }

    /**
     * @param string $event
     * @return string
     */
    public function generateHesh($event)
    {
        return md5(time() . $this->_salt);
    }

    /**
     * @param string $room
     * @return $this
     */
    public function to($room)
    {
        if(!isset($this->_rooms['$room'])){
            $this->_rooms[] = $room;
        }
        return $this;
    }

    /**
     * @param string $event
     * @param mixed $data
     * @param bool $final
     * @throws \Exception
     */
    public function send($event, $data, $final = true)      //Emit  на сервер
    {
        $dataString = '42' . json_encode([$event, $data]);
        $hesh = $this->generateHesh($event);
        $this->_client = new Client($this->url . '&hesh=' . $hesh);
        $this->_client->write($dataString, $final);
    }

    /**
     * @param bool $wait_for_end
     * @return mixed
     */
    public function read($wait_for_end = true)
    {
        if($this->_client){
            return $this->_client->read($wait_for_end);
        }
    }

    /**
     * @param string $event
     * @param mixed $data
     * @return mixed
     * @throws \Exception
     */
    public function ask($event, $data)
    {
        $this->send($event, $data);
        $str = $this->_client->read();
        while(substr($str, 0, 2) != 42){
            $str = $this->_client->read();
        }
        $str = preg_replace( '`^42`', '', $str);
        return json_decode($str, true, 5);
    }
}
