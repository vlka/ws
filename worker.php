<?php
namespace vlka\ws;
require_once __DIR__ . '/../../../vendor/autoload.php';

use Workerman\Worker;
use vlka\ws\Server;

$server = new Server(2024);
$server->init();

Worker::runAll();
