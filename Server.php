<?php

namespace vlka\ws;

use PHPSocketIO\Socket;
use PHPSocketIO\SocketIO;
use vlka\ws\Emitter;

/**
 * Class Server
 * @package vlka\ws
 * @property SocketIO $io
 * @property integer $port
 * @property mixed $options
 */
class Server
{
    const ROOM_ALL = 'all';

    public $port;
    public $options;

    protected $io;
    protected $_salt = '';

    /**
     * Server constructor.
     * @param $port
     * @param array $options
     */
    public function __construct($port, $options = [])
    {
        $this->port = $port;
        if(isset($options['salt'])){
            $this->_salt = $options['salt'];
            unset($options['salt']);
        }
        $this->options = $options;
    }

    /**
     * @param Socket $socket
     * @return array
     */
    protected function getCookies(Socket $socket)
    {
        if(isset($socket->request->headers['cookie'])){
            $cookies = explode('; ', $socket->request->headers['cookie']);
            $res = [];
            foreach ($cookies as $cookie) {
                $arr = explode('%22', $cookie);
                if(count($arr)>3){
                    $res[$arr[1]] = $arr[3];
                } else {
                    $arr = explode('=', $cookie);
                    $res[$arr[0]] = $arr[1];
                }
            }
            return $res;
        } else {
            return [];
        }
    }

    /**
     *
     */
    public function init()
    {
        $this->io = new SocketIO($this->port, $this->options);
        $this->io->on('connection', function ($socket){
            /** @var $socket Socket */
            if($this->chekHesh($socket)) { // Соединеия от локального сокета
                $socket->local = true;
                $socket->on(Emitter::EVENT_EMIT_ALL, function ($data) use ($socket) {
                    $socket->to(self::ROOM_ALL)->emit($data['name'], $data['data']);
                });
                $socket->on(Emitter::EVENT_TO, function ($data) use ($socket) {
                    foreach ($data['rooms'] as $room) {
                        $socket->to($room);
                    }
                    $socket->emit($data['name'], $data['data']);
                });
            } else {    // Подключения
                $socket->local = false;
                $socket->join(self::ROOM_ALL);
                $socket->on('disconnect', function (){
                    
                });
            }
        });
    }

    /**
     * @param Socket $socket
     * @return bool
     */
    protected function chekHesh($socket)
    {
        if(isset($socket->request->_query['hesh'])){
            return (md5($socket->handshake['issued'] . $this->_salt) == $socket->request->_query['hesh']);
        }
        return false;
    }

    /**
     * @param string $event
     * @param callable $func
     */
    public function on($event, callable $func)
    {
        if($this->io){
            $this->io->on($event, function($data) use ($func) {
                call_user_func($func, $data);
            });
        }
    }

    public function out($res)
    {
        if(is_string($res)){
            echo $res . "\n";
        } else {
            echo print_r($res, true);
        }
    }
}
