Php websocket server and client based workerman/phpsocket.io
============================================================
Php websocket server and client based workerman/phpsocket.io

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist vlka/ws "dev-master"
```

or add

```
"vlka/ws": "dev-master"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

#worker.php

```php
<?php
namespace vlka\ws;
require_once __DIR__ . '/../../../vendor/autoload.php';

use Workerman\Worker;
use vlka\ws\Server;

$server = new Server(2024);
$server->init();
$server->on('connection', function($socket){
    $socket->on('sameEvent', function($data) use($socket) {
    // TODO:
    });
});

Worker::runAll();
```
#console:
Start WebSocket server
```bash
php worker.php start
```
Start WebSocket server in DAEMON mode
```bash
php worker.php start -d
```
Display WebSocket status
```bash
php worker.php status
```
Display Connections
```bash
php worker.php connections
```
Stop WebSocket server
```bash
php worker.php stop
```
Restart WebSocket server
```bash
php worker.php restart
```
Restart WebSocket server in DAEMON mode
```bash
php worker.php restart -d
```
Reload WebSocket server
```bash
php worker.php reload
```


#MyController.php
```php
<?php
//  ...
use vlka\ws\Emitter;
//  ...

$socket = new Emitter(2024);
$socket->emit('sameEvent', ['foo' => 'bar']);

$socket->to('myRoom')->emit('sameEvent', ['foo' => 'bar']);

// Send to server
$socket->send('sameEvent', $data);

// Ask and wait answer
$answer = $socket->ask('sameEvent', $data);
```
